$(function() {
  var events;
  var markers = [];
  var map = L.map('map').setView([20, 0], 2);
  var indexCount = 0;

  L.tileLayer('http://{s}.tiles.mapbox.com/v3/gi0cann.kdh26816/{z}/{x}/{y}.png', {
    maxZoom: 18
  }).addTo(map);

  function onMapClick(e) {
    console.log(e.latlng);
    L.marker([e.latlng.lat, e.latlng.lng]).addTo(map);
  }

  function addMarker(element, index, array) {
    try {
      var m = L.marker([element.lat, element.long]).addTo(map);
      m.bindPopup("<b>"+ element.event + "</b><br>" + element.city);
      markers.push(m);
    }
    catch (e) {
      console.log(e);
    }
  }

  function animateMap(index) {
   // map.setView([events[index].lat, events[index].long], 5);
    markers[index].openPopup();
  }

  var events = (function () {
    var json = null;
    $.ajax( "json/events.json",
    {
      'async': false,
      'global': false,
      'dataType': "json",
      'success': function( data ) {
          json = data;
      }
    });
    return json;
  })();

  $("#events-timeline").append('<ul id="events" style="width: ' + events.length * 131 +
                    'px"></ul>');
  for (var i = 0; i < events.length; i++) {
    $("#events").append('<li class="btn btn-default" data-lat="' +
                         events[i].lat + '" data-long="' + events[i].long +
                         '" data-index="' + i + '">' + events[i].event + '</li>');
  }

  $('body').on('click', 'li', function() {
    map.setView([$( this ).data("lat"), $( this ).data("long")], 5);
    markers[$( this ).data("index")].openPopup();
  });


  btns = $(".btn");

  events.forEach(addMarker);

  setInterval(function() {
      animateMap(indexCount);
      indexCount = indexCount + 1;
  }, 2000);


});
